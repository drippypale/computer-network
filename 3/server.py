import io
import os
import socket
import pathlib
import time
import zlib
import struct
from enum import Enum

class ResponseMessage(Enum):
'''
An Enum class which represents the response message.
'''
    SUCCESS = '200 OK'
    FAILED  = '404 NOT FOUND'


class MyServer:
'''
A custom class to handle socket server stuffs for this specific case.

- receive the requested file name
- search for the file
- send a response message
- send the file if it exists.
'''
    def __init__(self,
            host='localhost', 
            port=6000, 
            bind=False, 
            server_files_path=os.path.join(pathlib.Path().absolute(), 'server_files')):
        self._server_files_path = server_files_path
        self._HOST = host
        self._PORT = port
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        if bind:
            self.socket.bind((self._HOST, self._PORT))
    
    def listen(self, backlog=2):
        '''
        same as Socket.listen()
        '''
        self.socket.listen(backlog)
        print('listening on the %s ...' % ((self._HOST, self._PORT), ))

    def accept(self):
        '''
        same as Socket.accept()
        '''
        self.conn, self.addr = self.socket.accept()
        print('Connected by ', self.addr)

    def get_file_name(self, chunk_size=1024):
        '''
        receive the requested file name from the client.

        chunk_size: size of the chunks.
        '''
        file_name = ''
        while True:
            chunk_data = self.conn.recv(chunk_size)
            if not chunk_data:
                break
            file_name += chunk_data.decode('utf-8', errors='ignore')
            if len(chunk_data) < chunk_size:
                break
        return file_name

    def search_file(self, file_name):
        '''
        return True if the given file_name exists in the server files directory.

        file_name: name of the file
        '''
        print('Search for \'%s\' ...' % file_name)
        return os.path.exists(os.path.join(self._server_files_path,  file_name))

    def read_file_in_chunks(self, file_name, chunk_size=1024):
        '''
        this generator method will open the given file, compress it if it's html file
        finally yield the file chunks.

        file_name: name of the file
        chunk_size: size of the chunks
        '''
        compress = zlib.compressobj(zlib.Z_DEFAULT_COMPRESSION, zlib.DEFLATED, +15)
        with open(os.path.join(self._server_files_path, file_name), 'rb') as requested_file:
            file_data = requested_file.read()
            if file_name.split('.')[-1] == 'html': 
                print('Compressing file ...')
                compressed_data = zlib.compress(file_data, zlib.Z_BEST_COMPRESSION)
                print('Turned %d bytes to the %d (compress ratio: %d%%)' % (len(file_data), len(compressed_data), (float(len(file_data)) - float(len(compressed_data))) / float(len(file_data)) * 100.0))
                file_data = compressed_data

            with io.BytesIO(file_data) as file_stream:
                while True:
                    data = file_stream.read(chunk_size)
                    if not data:
                        break
                    yield data 

    def send_file_in_chunks(self, file_name, chunk_size=1024, sleep=0.02):
        '''
        this method will call the read_file_in_chunks method to receive the next chunk.
        then sending it to the client by calling the .send

        file_name: name of the file which cliend requested
        chunk_size: size of the chunks
        sleep: delay between sending each chunk(for visualization purposes)
        '''
        total_bytes = 0
        for file_chunk in self.read_file_in_chunks(file_name, chunk_size):
            self.conn.send(file_chunk)
            time.sleep(sleep)
            total_bytes += len(file_chunk)
        print('Successfully sent to the %s.' % (self.addr, ))

    def send_message(self, msg: str):
        '''
        this method will send back the search result.
        also prefixes the size of the message so the client can calculate how many
        bytes are exacty for message so the rest is the requested file bytes.

        msg: the string message to send back to the client.
        '''
        msg = msg.encode()
        msg = struct.pack('I', len(msg)) + msg
        self.conn.sendall(msg)


if __name__ == '__main__':
    server = MyServer(bind=True)
    server.listen()
    while True:
        server.accept()
        file_name = server.get_file_name()
        if server.search_file(file_name):
            print('\'%s\' found. sending file back to the %s ...' % (file_name, server.addr))
            server.send_message(ResponseMessage.SUCCESS.value)
            server.send_file_in_chunks(file_name)
        else:
            print('\'%s\' not found.' % (file_name, ))
            server.send_message(ResponseMessage.FAILED.value)
        server.conn.close()
            
