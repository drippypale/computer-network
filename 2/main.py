import os
from urllib import request
from urllib.error import URLError
import pathlib
directory = pathlib.Path().absolute()

url_list = [
        'http://www.google.com',
        'http://www.youtube.com',
        'http://www.yazd.ac.ir',
        'http://www.digikala.com',
        'http://www.stackoverflow.com',
        'http://www.pypi.org',
        'http://www.github.com',
        'http://www.gitlab.com',
        'http://www.docker.io',
        'http://www.whatismyip.com',
]

def write_in_file(url: str, data):
    with open(os.path.join(directory, '%s.html' % url.split('/')[2]), 'w', encoding='utf-8') as my_file:
        my_file.write(data)
if __name__ == '__main__':
    for url in url_list:
        try:
            page = request.urlopen(url)
            write_in_file(url, page.read().decode("utf-8", errors='ignore'))
            print('\'%s\' successfully fetched ...' % url)
        except URLError:
            print('Error in fetching \'%s\' ...' % url)

