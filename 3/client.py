import os
import socket
import pathlib
import zlib
import struct

directory = pathlib.Path().absolute()

HOST = 'localhost'
PORT = 6000

file_name, path = input('file name to fetch: '), input('abs path to save the file: ')
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((HOST, PORT))
    print('connected to the %s' % ((HOST, PORT), ))
    s.sendall(file_name.encode())
    print('request for \'%s\' successfully sent' % file_name)
    result = bytearray()
    print('waiting for server response ...')
    size = struct.unpack('I', s.recv(struct.calcsize('I')))[0]
    while len(result) < size:
        chunk_data = s.recv(min(1024, size - len(result)))
        result += chunk_data
    result = result.decode('utf-8', errors='ignore')
    print(result)
    if result == '200 OK':
        print('fetching the file ...')
        file_content, total_bytes = bytearray(), 0
        while True:
            file_chunk = s.recv(1024)
            if not file_chunk:
                break
            total_bytes += len(file_chunk)
            print('%d bytes fetched ...' % total_bytes, end='\r')
            file_content.extend(file_chunk)
        print('the file successfully fetched. total bytes : %d' % total_bytes)
        if file_name.split('.')[-1] == 'html':
            print('decompressing file ...', end='\r')
            file_content = zlib.decompress(file_content)
            print('file successfully decompressed')
        print('saving response to the \'%s/%s\' ...' % (path, file_name), end='\r')
        with open(os.path.join(path, file_name), 'w') as my_file:
            my_file.write(file_content.decode('utf-8', errors='ignore'))
        print('the file successfully saved to the \'%s/%s\'' % (path, file_name))
