import datetime

if __name__ == '__main__':
    now = datetime.datetime.now()
    print('Today is %d/%d/%d' % (now.month, now.day, now.year))
    print('Today is {0}/{1}/{2}'.format(now.month, now.day, now.year))
