def is_perfect(num):
    divisors_sum = 0
    for i in range(1, num):
        if num % i == 0:
            divisors_sum += i
    return True if num == divisors_sum and divisors_sum != 0 else False


if __name__ == '__main__':
    num, perfect_count = int(input()), 0
    for i in range(2, num):
        if is_perfect(i):
            perfect_count += 1
    print(perfect_count)
